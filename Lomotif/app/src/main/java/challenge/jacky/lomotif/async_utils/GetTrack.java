package challenge.jacky.lomotif.async_utils;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import challenge.jacky.lomotif.models.Track;

/**
 * Created by JackyNguyen on 9/15/2015.
 * A class that inherits from AsyncTask to connect to WebApi and parse JSON to return array list
 */
public class GetTrack extends AsyncTask<String, Void, ArrayList<Track>> {
    private ArrayList<Track> tracks = new ArrayList<Track>();

    @Override
    protected ArrayList<Track> doInBackground(String... params) {

        String urlString = params[0];
        String result = "";
        InputStream in = null;

        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader r = new BufferedReader((new InputStreamReader(in)));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                sb.append(line);
            }
            result   = sb.toString();
            result = result.replace("\\", "");

            JSONObject root = new JSONObject(result);
            JSONArray array = root.getJSONArray("tracks");
            for(int i = 0; i < array.length(); i++){
                JSONObject obj = array.getJSONObject(i);
                Track track = new Track(obj);
                tracks.add(track);
            }
        }
        catch (Exception e){

        }

        return tracks;
    }
}


