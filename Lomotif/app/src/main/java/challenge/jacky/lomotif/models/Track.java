package challenge.jacky.lomotif.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by JackyNguyen on 9/15/2015.
 * Track object accept a JSONObject then get the string of that object then assign to member variable
 */
public class Track {
    private String posterImg;
    private String song;
    private String artist;

    public Track(JSONObject jObject) throws JSONException{
        posterImg = jObject.getString("artwork_url");
        song = jObject.getString("track_name");
        artist  = jObject.getString("artist_name");
    }

    public String getPosterImg() {
        return posterImg;
    }

    public void setPosterImg(String posterImg) {
        this.posterImg = posterImg;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
}
