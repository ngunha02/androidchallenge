package challenge.jacky.lomotif;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import challenge.jacky.lomotif.async_utils.GetTrack;
import challenge.jacky.lomotif.models.Track;


public class MainActivity extends Activity {

    private ArrayList<Track> tracks;
    private ListView trackListLV;
    private  CustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        trackListLV = (ListView)this.findViewById(R.id.trackList);

        try{
            tracks = new GetTrack().execute("http://charts.spotify.com/api/tracks/most_streamed/global/daily/latest").get();
            customAdapter = new CustomAdapter(this, tracks);
            trackListLV.setAdapter(customAdapter);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
