package challenge.jacky.lomotif.async_utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import challenge.jacky.lomotif.models.Track;

/**
 * Created by JackyNguyen on 9/15/2015.
 * A class that inherits from AsyncTask to connect to WebApi and parse JSON to return a single bitmap instance
 */
public class GetPoster extends AsyncTask<String, Void, Bitmap> {
    private Bitmap bitmap = null;

    @Override
    protected Bitmap doInBackground(String... params) {

        String urlString = params[0];
        String result = "";
        InputStream in = null;

        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream(), 1024);
            bitmap = BitmapFactory.decodeStream(in);
        }
        catch (Exception e){

        }
        return bitmap;
    }
}