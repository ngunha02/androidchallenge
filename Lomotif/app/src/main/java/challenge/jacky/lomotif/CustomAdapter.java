package challenge.jacky.lomotif;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import challenge.jacky.lomotif.async_utils.GetPoster;
import challenge.jacky.lomotif.models.Track;

/**
 * Created by JackyNguyen on 9/15/2015.
 * CustomAdapter is used for list view in main activity
 */
public class CustomAdapter extends ArrayAdapter<Track> {

    public  CustomAdapter(Context context, ArrayList<Track> tracks){
        super(context, 0, tracks);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater li = (LayoutInflater) getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.track_view, null);
        }

        Track t = getItem(position);

        TextView song = (TextView) convertView.findViewById(R.id.song);
        song.setText(t.getSong());

        TextView artist = (TextView) convertView.findViewById(R.id.artist);
        artist.setText(t.getArtist());

        ImageView posterImg = (ImageView) convertView.findViewById(R.id.posterImg);
        try{
            Bitmap result = new GetPoster().execute(t.getPosterImg()).get();
            posterImg.setImageBitmap(result);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return  convertView;
    }
}
